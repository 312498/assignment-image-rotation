#ifndef _FILE_H_
#define _FILE_H_

#include <stdio.h>

enum open_status {
    OPEN_NULL_NAME,
    OPEN_ERROR,
    OPEN_OK,
};

enum close_status {
    CLOSE_OK,
    CLOSE_ERROR
};

enum close_status close_file(FILE* f);
enum open_status open_file_to_read(const char* name, FILE** file);
enum open_status open_file_to_write(const char* name, FILE** file);
#endif
