#ifndef _BMP_STRUCT_H_
#define _BMP_STRUCT_H_

#include "file.h"
#include "image_struct.h"
#include <stdint.h>
#include <stdlib.h>

enum read_status {
    READ_OK,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR = 1
};

enum read_status read_image_from_file(char* file_name, struct image_struct* img);
enum write_status write_image_in_file(char* file_name, struct image_struct* img);
#endif
