
#ifndef _ROTATE_TRANSFORMATION_H_
#define _ROTATE_TRANSFORMATION_H_
#include "image_struct.h"

struct image_struct rotate(struct image_struct source);

#endif
