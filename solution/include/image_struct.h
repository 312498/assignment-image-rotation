#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t r, g, b;
};

struct image_struct {
    uint64_t width, height;
    struct pixel* data;
};

struct image_struct create_image(uint64_t width, uint64_t height);
struct pixel* image_row_addr(const struct image_struct* img, uint64_t row);
struct pixel* get_pixel(struct image_struct* img, uint64_t i, uint64_t j);
void free_image(struct image_struct* image);

#endif


