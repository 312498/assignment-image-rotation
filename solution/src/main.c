#include "bmp_struct.h"
#include "rotate_transformation.h"

int main( int argc, char** argv ) {
    if (argc < 3) return 1;
    if (argc > 3) return 1;

    char* input_file = argv[1];
    char* output_file = argv[2];

    struct image_struct img = {0};

    enum read_status status = read_image_from_file(input_file, &img);
    if (status == READ_OK) {
        struct image_struct new_img = rotate(img);
        free_image(&img);
        write_image_in_file(output_file, &new_img);
        free_image(&new_img);
    } else {
        return 1;
    }

    return 0;
}
