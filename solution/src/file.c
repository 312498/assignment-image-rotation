#include "../include/file.h"
#include <stdint.h>

enum close_status close_file(FILE* f) {
    if (!f) return CLOSE_ERROR;
    uint8_t res = fclose(f);
    if (res == 0) {
        return CLOSE_OK;
    } else {
        return CLOSE_ERROR;
    }
}

enum open_status open_file_to_read(const char* name, FILE** file) {
    if (!name) return OPEN_NULL_NAME;
    if(!file) return OPEN_ERROR;
    *file = fopen(name, "rb");
    if (!*file) return OPEN_ERROR;
    return OPEN_OK;
}

enum open_status open_file_to_write(const char* name, FILE** file) {
    if (!name) return OPEN_NULL_NAME;
    *file = fopen(name, "wb");
    if (!*file) return OPEN_ERROR;
    return OPEN_OK;
}
