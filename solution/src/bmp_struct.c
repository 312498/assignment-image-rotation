#include "bmp_struct.h"

#define FOR_BMP_STRUCT( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bf_type)\
        FOR_FIELD( uint32_t,bfile_size)\
        FOR_FIELD( uint32_t,bf_reserved)\
        FOR_FIELD( uint32_t,b_off_bits)\
        FOR_FIELD( uint32_t,bi_size)\
        FOR_FIELD( uint32_t,bi_width)\
        FOR_FIELD( uint32_t,bi_height)\
        FOR_FIELD( uint16_t,bi_planes)\
        FOR_FIELD( uint16_t,bi_bit_count)\
        FOR_FIELD( uint32_t,bi_compression)\
        FOR_FIELD( uint32_t,bi_size_image)\
        FOR_FIELD( uint32_t,bi_x_pels_per_meter)\
        FOR_FIELD( uint32_t,bi_y_pels_per_meter)\
        FOR_FIELD( uint32_t,bi_clr_used)\
        FOR_FIELD( uint32_t,bi_clr_important)

#define DECLARE_FIELD( t, n ) t n ;

struct __attribute__((packed)) bmp_struct
{
    FOR_BMP_STRUCT( DECLARE_FIELD )
};

#define BFTYPE1 19778
#define BFTYPE2 0x424D
#define BFRESERVED 0
#define BFOFFBITS 54
#define BISIZE 40
#define BIPLANES 1
#define BIBITCOUNT 24
#define BICOMPRESSION 0
#define BIPELSPERMETER 2834
#define BICLRUSED 0
#define BICLRIMPORTANT 0

static enum read_status check_bmp_header(const struct bmp_struct* bmp) {
    if ((bmp -> bf_type != (uint16_t) BFTYPE1) && (bmp -> bf_type != BFTYPE2)) {
        return READ_INVALID_HEADER;
    } else if (bmp -> bi_bit_count != BIBITCOUNT) {
        return READ_INVALID_BITS;
    }
    return READ_OK;
}

static uint8_t count_padding(const uint64_t width) {
    const uint8_t padding = 4 - ((width * 3) % 4);
    if (padding == 4) {
        return 0;
    } else {
        return padding;
    }
}

static struct bmp_struct create_bmp_from_img(const struct image_struct *img) {
    uint64_t width = img->width;
    uint64_t height = img->height;
    const uint32_t bi_size_image = (width * sizeof(struct pixel) + count_padding(width)) * height;
    const uint32_t bi_file_size = bi_size_image + sizeof(struct bmp_struct);
    return (struct bmp_struct) {
            BFTYPE1, bi_file_size,
            BFRESERVED, BFOFFBITS, BISIZE,
            width, height, BIPLANES, BIBITCOUNT, BICOMPRESSION,
            bi_size_image, BIPELSPERMETER, BIPELSPERMETER, BICLRUSED, BICLRIMPORTANT
    };
}

static enum read_status from_bmp( FILE* in, struct image_struct* img ) {
    struct bmp_struct bmp = {0};
    if (!fread( &bmp, sizeof( struct bmp_struct ), 1, in)) {
        return READ_ERROR;
    }
    enum read_status status =  check_bmp_header(&bmp);
    if (status != READ_OK) {
        free_image(img);
        return status;
    }
    *img = create_image(bmp.bi_width, bmp.bi_height);
    if (!img->data) {
        free_image(img);
        return READ_ERROR;
    }

    const uint8_t padding = count_padding(img->width);

    for (size_t i = 0; i < img -> height; i++) {
        if(!fread(image_row_addr(img, i),(size_t) (img->width) * sizeof(struct pixel), 1, in)) {
            free_image(img);
            return READ_ERROR;
        }
        if (fseek(in, padding, SEEK_CUR) != 0) {
            free_image(img);
            return READ_ERROR;
        }
    }

    return READ_OK;
}

enum read_status read_image_from_file(char* file_name, struct image_struct* img) {
    FILE* input_file = NULL;
    enum open_status open_status = open_file_to_read(file_name, &input_file);
    if (open_status != OPEN_OK) {
        return READ_ERROR;
    }
    enum read_status read_status = from_bmp(input_file, img);
    enum close_status close_status = close_file(input_file);
    if (close_status != CLOSE_OK) {
        return READ_ERROR;
    }
    return read_status;
}

enum write_status to_bmp( FILE* out, struct image_struct const* img ) {
    struct bmp_struct bmp = create_bmp_from_img(img);

    size_t result = fwrite(&bmp, sizeof(struct bmp_struct), 1, out);
    if (!result) {
        return WRITE_ERROR;
    }
    const uint8_t padding = count_padding(img->width);

    uint32_t bytes_for_padding = 3;

    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(image_row_addr(img, i), img->width * sizeof(struct pixel), 1, out)) {
            return WRITE_ERROR;
        }
        if (!fwrite(&bytes_for_padding, padding, 1, out)) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

enum write_status write_image_in_file(char* file_name, struct image_struct* img) {
    FILE* output_file = NULL;
    enum open_status open_status = open_file_to_write(file_name, &output_file);
    if (open_status != OPEN_OK) {
        return WRITE_ERROR;
    }
    enum write_status write_status = to_bmp(output_file, img);
    enum close_status close_status = close_file(output_file);
    if (close_status != CLOSE_OK) {
        return WRITE_ERROR;
    }
    return write_status;
}


