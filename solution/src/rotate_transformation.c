
#include "../include/image_struct.h"

struct image_struct rotate (struct image_struct source) {
    struct image_struct new_img = create_image(source.height, source.width);

    for (uint64_t i = 0; i < source.height; i++) {
        for (uint64_t j = 0; j < source.width; j++) {
            *(new_img.data + new_img.width * j + (new_img.width - i - 1)) = *get_pixel(&source, i, j);
        }
    }

    return new_img;
}
