#include "image_struct.h"
#include <stdint.h>
#include <stdlib.h>

struct image_struct create_image(uint64_t width, uint64_t height) {
    struct image_struct img = {0};
    img.width = width;
    img.height = height;
    img.data = malloc(sizeof(struct pixel) * img.width * img.height);
    return img;
}

struct pixel* image_row_addr(const struct image_struct* img, uint64_t row) {
    return img -> data + (img-> width * row);
}

struct pixel* get_pixel(struct image_struct* img, uint64_t i, uint64_t j) {
     return img->data + img->width * i + j;
}

void free_image(struct image_struct* image) {
    free(image->data);
}



